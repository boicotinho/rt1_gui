import asyncio
import logging
from typing import ItemsView, Iterator, List, Optional, Tuple

from .types import ICompetitor, Instrument
from .messages import *

MAX_DATAGRAM_SIZE = 508
MAX_TRADE_TICKS = (MAX_DATAGRAM_SIZE - TRADE_TICKS_HEADER_SIZE) // TRADE_TICK_SIZE


class InformationChannel(asyncio.DatagramProtocol):
    def __init__(self, remote_address: Optional[Tuple[str, int]] = None):
        """Initialize a new instance of the InformationChannel class."""
        self.remote_address: Optional[Tuple[str, int]] = remote_address
        self.transport: Optional[asyncio.DatagramTransport] = None

        self.logger: logging.Logger = logging.getLogger("INFORMATION")

        self.book_message = bytearray(ORDER_BOOK_MESSAGE_SIZE)
        HEADER.pack_into(self.book_message, 0, ORDER_BOOK_MESSAGE_SIZE, MessageType.ORDER_BOOK_UPDATE)

        self.ticks_message = bytearray(MAX_DATAGRAM_SIZE)
        self.tcp_listenners: List[asyncio.Transport] = []

    def connection_made(self, transport: asyncio.BaseTransport) -> None:
        """Called when the datagram endpoint is created."""
        self.transport = transport

    def add_marketdata_tcp_endpoint(self, trp: asyncio.Transport) -> None:
        """Called when a click (manual) trader logs in."""
        self.tcp_listenners.append(trp)

    def remove_tcp_listenner(self, trp: asyncio.Transport) -> None:
        """Called when a click (manual) trader logs loses connection"""
        if trp != None:
            self.tcp_listenners.remove(trp)

    def send_order_book_update(self, instrument: int, sequence_number: int, ask_prices: List[int],
                               ask_volumes: List[int], bid_prices: List[int], bid_volumes: List[int]) -> None:
        """Send an order book update message to the auto-trader."""
        ORDER_BOOK_HEADER.pack_into(self.book_message, HEADER_SIZE, instrument, sequence_number)
        ORDER_BOOK_MESSAGE.pack_into(self.book_message, ORDER_BOOK_HEADER_SIZE, *ask_prices, *ask_volumes, *bid_prices,
                                     *bid_volumes)
        self.transport.sendto(self.book_message, self.remote_address)
        self.relay_to_tcp_listenners(self.book_message)

    def send_trade_ticks(self, instrument: int, trade_ticks: ItemsView[int, int]) -> None:
        """Send a trade ticks message to the auto-trader."""
        message = self.ticks_message

        count = len(trade_ticks)
        if count > MAX_TRADE_TICKS:
            count = MAX_TRADE_TICKS

        offset: int = TRADE_TICKS_HEADER_SIZE
        size: int = offset + count * TRADE_TICK_SIZE

        HEADER.pack_into(message, 0, size, MessageType.TRADE_TICKS)
        TRADE_TICKS_HEADER.pack_into(message, HEADER_SIZE, instrument)

        ticks: Iterator[Tuple[int, int]] = iter(trade_ticks)
        while offset < size:
            TRADE_TICK.pack_into(message, offset, *next(ticks))
            offset += TRADE_TICK_SIZE

        self.transport.sendto(message[:size], self.remote_address)
        self.relay_to_tcp_listenners(message[:size])

    def relay_to_tcp_listenners(self, a_msg) -> None:
        """Send market data to TCP click tranders."""
        for vv in self.tcp_listenners:
            try:
                vv.write(a_msg)
            except:
                self.tcp_listenners.remove(vv)

    def send_pnl_snapshot(self, competitors: List[ICompetitor]) -> None:
        if len(self.tcp_listenners) == 0:
            return
        # build pnl snapshot
        num_comps = len(competitors)
        MSG_SIZE = PNL_DATA_HEADER_SIZE + PNL_DATA_SIZE * num_comps
        pnl_msg: bytearray = bytearray(MSG_SIZE)
        HEADER.pack_into(pnl_msg, 0, MSG_SIZE, MessageType.MARKET_PNL)
        PNL_DATA_HEADER.pack_into(pnl_msg, HEADER_SIZE, num_comps)
        ofs = 0
        for comp in competitors:
            account = comp.get_account_info()
            pid     = int(account.get_account_id())
            pos_etf = int(account.get_position(Instrument.ETF))
            balance = int(account.get_balance())
            pnl     = int(account.get_pnl())
            fees    = int(account.get_fees())
            #print('xx %d -> %d,%d,%d,%d' %(PNL_DATA_HEADER_SIZE + ofs, pid, pos_etf, balance, pnl) )
            PNL_DATA.pack_into(pnl_msg, PNL_DATA_HEADER_SIZE + ofs, pid, pos_etf, balance, pnl, fees)
            ofs += PNL_DATA_SIZE
        self.relay_to_tcp_listenners(pnl_msg)

