import sys
import os
import traceback
import json
import pathlib
import time

import socket
import asyncio
import logging
import struct
import random
import enum
from functools import reduce
from collections import deque
from typing import List, Optional, Text, Tuple, Union, Dict, Any

# Import Ready Trader messages
this_script_dir = os.path.dirname(os.path.realpath(__file__))
rt1_python_dir = os.path.abspath(os.path.join(this_script_dir, "../server_exchange/"))
sys.path.insert(1, rt1_python_dir)
from ready_trader_one.messages import *
from ready_trader_one.order_book import TOP_LEVEL_COUNT
from ready_trader_one.types import Lifespan, Side


class Profiler:
    g_perf_enable = False
    g_perf_csv = None
    g_perf_csv_sum = {}


def tperf(func):
    def function_timer(*args, **kwargs):
        if not Profiler.g_perf_enable:
            return func(*args, **kwargs)
        start = time.time()
        value = func(*args, **kwargs)
        end = time.time()
        runtime = end - start
        if func.__name__ not in Profiler.g_perf_csv_sum:
            Profiler.g_perf_csv_sum[func.__name__] = 0
        Profiler.g_perf_csv_sum[func.__name__] += runtime
        if Profiler.g_perf_csv is None:
            Profiler.g_perf_csv = open("perf.csv", "w")
        if not Profiler.g_perf_csv.closed:
            msg = "{ts},{func},{time}\n"
            Profiler.g_perf_csv.write(msg.format(ts=start, func=func.__name__, time=runtime))
        return value
    return function_timer

class Book:
    @tperf
    def __init__(self, inst, tick_size=100):
        self.instrument: int = inst  # 0: FUT, 1: ETF
        self.ask_prices = []
        self.ask_volumes = []
        self.bid_prices = []
        self.bid_volumes = []
        self.latest_seqno = 0
        self.curr_pos = 0
        self.tick_size = tick_size
        self.a_dict = {}
        self.b_dict = {}
        self.tplot_x_seqno = []
        self.tplot_y_mid_px = []
        self.tplot_y_pos = []

    @tperf
    def midpxf(self):
        if len(self.ask_prices) > 0 and len(self.bid_prices) > 0:
            return (self.ask_prices[0] + self.bid_prices[0]) / 2
        elif len(self.ask_prices) > 0:
            return (self.ask_prices[0]) / 1
        elif len(self.bid_prices) > 0:
            return (self.bid_prices[0]) / 1
        else:
            return 0

    @tperf
    def midpx(self):
        return self.tick_round(self.midpxf()) / 1

    @tperf
    def tick_round(self, px: float) -> int:
        ret = int(round(float(px) / float(self.tick_size)) * self.tick_size)
        return ret

    @tperf
    def on_order_book_update_message_cbook(
        self,
        sequence_number: int,
        ask_prices: List[int],
        ask_volumes: List[int],
        bid_prices: List[int],
        bid_volumes: List[int],
    ) -> None:
        self.latest_seqno = sequence_number
        self.ask_prices = list(filter(lambda y: y >= 0, ask_prices))
        self.ask_volumes = list(filter(lambda y: y >= 0, ask_volumes))
        self.bid_prices = list(filter(lambda y: y >= 0, bid_prices))
        self.bid_volumes = list(filter(lambda y: y >= 0, bid_volumes))
        self.a_dict = {}
        self.b_dict = {}
        self.a_mine = {}
        self.b_mine = {}
        for ii in range(len(self.ask_prices)):
            px = self.ask_prices[ii]
            qx = self.ask_volumes[ii]
            if qx > 0:
                self.a_dict[px] = qx
        for ii in range(len(self.bid_prices)):
            px = self.bid_prices[ii]
            qx = self.bid_volumes[ii]
            if qx > 0:
                self.b_dict[px] = qx
        mpx = self.midpx() / 100.0
        if mpx > 0:
            self.tplot_x_seqno.append(sequence_number)
            self.tplot_y_mid_px.append(mpx)
            self.tplot_y_pos.append(self.curr_pos)


class IMarketEventListenner(object):
    def on_error_message(self, client_order_id: int, error_message: bytes) -> None:
        pass

    def on_order_status_message(
        self, oid: int, fill: int, remaining: int, fees: int,
        side, lifespan, org_price, fill_price
    ) -> None:
        pass

    def on_order_book_update_message(self, book: Book) -> None:
        pass

    def on_position_change_message(
        self, future_position: int, etf_position: int
    ) -> None:
        pass

    def on_trade_ticks_message(
        self, instrument: int, trade_ticks: List[Tuple[int, int]]
    ) -> None:
        pass


def handle_exception(ex: Exception, context=None):
    xctx = "" if context is None or len(context) == 0 else (" while " + context)
    pbar = "#" * 120 + "\n"
    msg = pbar
    if type(ex) == Exception:  # Raised by us with no need for stacktrace
        msg += "Client threw an exception %s: %s\n" % (xctx, ex)
    else:  # Raised by a library and we'd like stracktrace info
        msg += "Client threw a '%s' exception %s: %s\n" % (xctx, type(ex).__name__, ex)
        msg += pbar
        traceback.print_exc(file=sys.stderr)
    print(msg, file=sys.stderr)
    # save_config_safe()
    logger = logging.getLogger("EX")
    logger.error("Exception: \n" + msg)
    if Profiler.g_perf_csv is not None:
        Profiler.g_perf_csv.close()
        fsum = open("fsum.csv", "w")
        for k, v in g_perf_csv_sum.items():
            fsum.write("%s,%s\n" % (k, repr(v)))
        fsum.close()


class Participant:
    def __init__(self, team_name):
        self.team_name = team_name
        self.pos_etf = 0
        self.balance = 0
        self.pnl = 0
        self.fees = 0

    @tperf
    def update_pnl(self, pos, bal, prof, fees):
        self.pos_etf = pos
        self.balance = bal
        self.pnl = prof
        self.fees = fees


g_participants = {}  # id -> Participant


class Order:
    @tperf
    def __init__(self, oid, instr, side, px, qx):
        self.oid = int(oid)
        self.instr = int(instr)
        self.side = Side(side)
        self.px = int(px)
        self.qx = int(qx)
        self.qx_org = self.qx
        self.qx_fil = 0
        self.pending_new = True
        if (self.instr not in [0, 1] or self.px <= 0 or self.px % 100 != 0 or self.qx <= 0):
            raise Exception(
                "Invalid order: %s, %s, %s, %s, %s"
                % (repr(oid), repr(instr), repr(side), repr(px), repr(qx))
            )


class Trade:
    @tperf
    def __init__(self, order: Order, fill: int, trade_px: int, exch_time: int):
        self.order = order
        self.trade_qx = int(fill)
        self.trade_px = int(trade_px)
        self.exch_time = int(exch_time)


class OrderManager:
    @tperf
    def __init__(self):
        self.next_oid = 1
        self.orders = {}
        self.trades = []
        self.clt = None
        self.exch_time = 0
        self.logger = logging.getLogger("OM")

    def bind_client(self, clt):
        self.clt = clt

    @tperf
    def qty_str_at_px(self, side: Side, px) -> int:
        qsum = 0
        for (oid, order) in self.orders.items():
            if order.px == px and order.side == side:
                qsum += order.qx
        if qsum == 0:
            return ""
        return "%d" % (qsum)

    @tperf
    def cancel_at_px(self, instrument: int, px: int) -> int:
        if px <= 0:
            return 0
        return self.cancel_all(instrument, True, True, px)

    @tperf
    def cancel_all(self, instrument: int, bids: bool, asks: bool, px: int = 0) -> int:
        if instrument != 1:
            self.logger.error("Only ETF(1) orders are supported")
            return 0
        count = 0
        for (oid, order) in self.orders.items():
            if px > 0 and order.px != px:
                continue
            if (not bids) and order.side != Side.BUY:
                continue
            if (not asks) and order.side != Side.SELL:
                continue
            if self.cancel_oid(oid):
                count += 1
        return count

    @tperf
    def cancel_oid(self, oid) -> bool:
        if oid not in self.orders:
            return False
        self.clt.send_cancel_order(oid)  # is on_order_status_message() sent?
        return True

    @tperf
    def new_order(
        self,
        instrument: int,
        side: Side,
        price: int,
        volume: int,
        tif: Lifespan = Lifespan.GOOD_FOR_DAY,
    ):  # 0:FOK, 1:GFD
        # print('New order: %d' % (self.next_oid))
        if instrument != 1:  # EFT(1) is the only tradeable instrument
            self.logger.error("Only ETF(1) orders are supported")
            return
        order = Order(self.next_oid, instrument, side, price, volume)
        self.clt.send_insert_order(order.oid, order.side, order.px, order.qx, tif)
        if tif == Lifespan.GOOD_FOR_DAY:
            self.orders[order.oid] = order
        self.next_oid += 1

    @tperf
    def set_exch_time(self, seqno):
        self.exch_time = seqno

    @tperf
    def on_order_status_message(self, oid: int, fill: int, remaining: int, fees: int,
                    side, lifespan, org_price, fill_price):
        #print('on_order_status_message: oid=%d, fill=%d, rem=%d)' %(oid,fill,remaining))
        if oid not in self.orders:
            self.logger.info("Order status from secondary login: oid=%d, fill=%d, rem=%d"%(oid,fill,remaining))
            order = Order(oid, 1, Side.BUY, org_price, fill+remaining)
            self.orders[oid] = order
        if oid >= self.next_oid:
            self.next_oid = oid + 1

        order = self.orders[oid]
        order.pending_new = False
        order.qx = remaining
        order.qx_fil += fill
        if fill > 0:
            self.trades.append(Trade(order, fill, fill_price, self.exch_time))
        if remaining <= 0:
            del self.orders[oid]

    @tperf
    def on_error_message(self, oid: int, error_message: str):
        if oid not in self.orders:
            return
        order = self.orders[oid]
        if order.pending_new:
            del self.orders[oid]


g_om = OrderManager()


@tperf
def validate_hostname(config, section, key):
    try:
        config[section][key] = socket.gethostbyname(config[section][key])
    except socket.error:
        raise Exception("Could not validate hostname in %s configuration" % section)


@tperf
def validate_json_object(config, section, required_keys, value_types):
    obj = config[section]
    if type(obj) is not dict:
        raise Exception("%s configuration should be a JSON object" % section)

    for rk in required_keys:
        if rk not in obj:
            raise Exception(
                "A required key is missing from the configuration: '%s'.'%s'"
                % (section, rk)
            )

    for key, typ in zip(required_keys, value_types):
        if type(obj[key]) is not typ:
            raise Exception(
                "Configuration type error for '%s'.'%s': expected %s got %s"
                % (section, key, repr(type(obj[key])), repr(typ))
            )


@tperf
def config_validator(config):
    if type(config) is not dict:
        raise Exception("Configuration file contents should be a JSON object")

    for required_key in ("Execution", "TeamName", "Secret"):
        if required_key not in config:
            raise Exception(
                "A required key is missing from the configuration: '%s'" % required_key
            )

    validate_json_object(config, "Execution", ("Host", "Port"), (str, int))
    validate_hostname(config, "Execution", "Host")

    if type(config["TeamName"]) is not str:
        raise Exception("TeamName has inappropriate type")
    if len(config["TeamName"]) < 1 or len(config["TeamName"]) > 50:
        raise Exception(
            "TeamName must be at least one, and no more than fifty, characters long"
        )

    if type(config["Secret"]) is not str:
        raise Exception("Secret has inappropriate type")
    if len(config["Secret"]) < 1 or len(config["Secret"]) > 50:
        raise Exception(
            "Secret must be at least one, and no more than fifty, characters long"
        )

    return True


@tperf
def load_config(a_file_name: str) -> Dict[str, Any]:
    ret_cfg: Dict[str, Any] = None
    logger = logging.getLogger("CFG")
    config_path = pathlib.Path(a_file_name)
    if config_path.exists():
        with config_path.open("r") as config:
            try:
                ret_cfg = json.load(config)
            except json.decoder.JSONDecodeError as ex:
                raise Exception(
                    "JSON decode error at %s:\n%s" % (config_path.resolve(), ex)
                )
        if not config_validator(ret_cfg):
            raise Exception(
                "configuration failed validation: %s" % config_path.resolve()
            )
    else:
        raise Exception("configuration file does not exist: %s" % str(config_path))

    logger.info("Started with arguments={%s}", ", ".join(sys.argv))
    if ret_cfg is not None:
        logger.info("configuration=%s", json.dumps(ret_cfg, separators=(",", ":")))
    return ret_cfg


@tperf
def save_config(a_file_name: str, cfg: Dict[str, Any]) -> None:
    logger = logging.getLogger("CFG")
    config_path = pathlib.Path(a_file_name)
    with open(config_path, "w") as f:
        json.dump(cfg, f, indent=2, sort_keys=False)


class ExitCode(enum.IntEnum):
    GRACEFUL = 0
    COULD_NOT_CONNECT = 1
    BAD_LOGIN = 2
    CLIENT_COMM_ERROR = 3


class ClickTradeClient(asyncio.Protocol):
    def __init__(
        self,
        a_loop,
        a_on_con_lost,
        a_team_name,
        a_password,
        a_events_cb: IMarketEventListenner,
    ):
        self.logger = logging.getLogger("TRADER")
        self.on_con_lost = a_on_con_lost
        self.tcp_conn: Optional[asyncio.Transport] = None
        self.events_cb = a_events_cb

        self.team_name: Optional[bytes] = a_team_name
        self.team_id = None
        self.secret: Optional[bytes] = a_password
        self.exit_code = ExitCode.COULD_NOT_CONNECT

        self._data: bytes = b""
        self.amend_message: bytearray = bytearray(AMEND_MESSAGE_SIZE)
        self.cancel_message: bytearray = bytearray(CANCEL_MESSAGE_SIZE)
        self.insert_message: bytearray = bytearray(INSERT_MESSAGE_SIZE)
        self.BOOK_PART = struct.Struct("!%dI" % (TOP_LEVEL_COUNT,))
        HEADER.pack_into(
            self.amend_message, 0, AMEND_MESSAGE_SIZE, MessageType.AMEND_ORDER
        )
        HEADER.pack_into(
            self.cancel_message, 0, CANCEL_MESSAGE_SIZE, MessageType.CANCEL_ORDER
        )
        HEADER.pack_into(
            self.insert_message, 0, INSERT_MESSAGE_SIZE, MessageType.INSERT_ORDER
        )

        self.books = [Book(0), Book(1)]
        g_om.bind_client(self)

    @tperf
    def connection_made(self, transport):  # from asyncio.Protocol
        message = HEADER.pack(
            LOGIN_MESSAGE_SIZE, MessageType.LOGIN
        ) + LOGIN_MESSAGE.pack(self.team_name.encode(), self.secret.encode(), True)
        transport.write(message)
        self.exit_code = ExitCode.BAD_LOGIN

    @tperf
    def exit_client(self, ec: ExitCode):
        self.exit_code = ec
        self.on_con_lost.set_result(True)

    @tperf
    def connection_lost(self, exc):  # from asyncio.Protocol
        self.logger.info("The server closed the connection")
        self.on_con_lost.set_result(True)

    @tperf
    def on_clientstate_message(self, new_state: ClientState):
        if new_state == ClientState.LOGGED_IN:
            self.logger.info("Login accepted")
            self.exit_code = ExitCode.GRACEFUL
        elif new_state == ClientState.TRADE_OPEN:
            self.logger.info("Market open!")
        else:
            self.logger.error("Unknown state: %s" % (new_state))

    @tperf
    def on_error_message(self, client_order_id: int, error_message: str):
        self.logger.error("Error message from exchange: %s" % (error_message))
        g_om.on_error_message(client_order_id, error_message)
        self.events_cb.on_error_message(client_order_id, error_message)

    @tperf
    def on_position_change_message(
        self, future_position: int, etf_position: int
    ) -> None:
        self.books[0].curr_pos = future_position
        self.books[1].curr_pos = etf_position
        self.events_cb.on_position_change_message(future_position, etf_position)

    @tperf
    def on_order_book_update_message(
        self,
        instrument_id: int,
        sequence_number: int,
        ask_prices: List[int],
        ask_volumes: List[int],
        bid_prices: List[int],
        bid_volumes: List[int],
    ) -> None:
        book = self.books[instrument_id]
        book.on_order_book_update_message_cbook(
            sequence_number, ask_prices, ask_volumes, bid_prices, bid_volumes
        )
        if instrument_id == 1:
            g_om.set_exch_time(sequence_number)
        self.events_cb.on_order_book_update_message(book)

    def on_order_status_message(self, oid: int, fill: int, remaining: int, fees: int,
                    side, lifespan, org_price, fill_price):
        self.logger.info(
            "Order status: oid=%d, fill=%d, remaining=%d, fees=%d, side=%s, lifespan=%s, org_price=%d, fill_price=%d"
            % (oid, fill, remaining, fees, str(side), str(lifespan), org_price, fill_price)
        )
        g_om.on_order_status_message(oid, fill, remaining, fees, side, lifespan, org_price, fill_price)
        self.events_cb.on_order_status_message(oid, fill, remaining, fees, side, lifespan, org_price, fill_price)

    @tperf
    def on_trade_ticks_message(
        self, instrument: int, trade_ticks: List[Tuple[int, int]]
    ) -> None:
        self.events_cb.on_trade_ticks_message(instrument, trade_ticks)

    @tperf
    def send_amend_order(self, client_order_id: int, volume: int) -> None:
        AMEND_MESSAGE.pack_into(
            self.amend_message, HEADER_SIZE, client_order_id, volume
        )
        self.tcp_conn.write(self.amend_message)

    @tperf
    def send_cancel_order(self, client_order_id: int) -> None:
        CANCEL_MESSAGE.pack_into(self.cancel_message, HEADER_SIZE, client_order_id)
        self.tcp_conn.write(self.cancel_message)

    @tperf
    def send_insert_order(
        self,
        client_order_id: int,
        side: Side,
        price: int,
        volume: int,
        lifespan: Lifespan,
    ) -> None:
        INSERT_MESSAGE.pack_into(
            self.insert_message,
            HEADER_SIZE,
            client_order_id,
            side,
            price,
            volume,
            lifespan,
        )
        self.tcp_conn.write(self.insert_message)

    @tperf
    def on_new_participant(self, pid: int, team_name: str) -> None:
        if team_name == self.team_name:
            self.team_id = pid
        g_participants[pid] = Participant(team_name)

    @tperf
    def on_pnl(self, pnl_items):
        for pid, pos_etf, balance, pnl, fees in pnl_items:
            g_participants[pid].update_pnl(pos_etf, balance, pnl, fees)

    @tperf
    def data_received(self, data):  # from asyncio.Protocol
        if self._data:
            self._data += data
        else:
            self._data = data
        try:
            upto = 0
            while upto < len(self._data) - HEADER_SIZE:
                length, typ = HEADER.unpack_from(self._data, upto)
                # print("inbound msg type=%d, size=%d" %(typ, length))
                if upto + length > len(self._data):
                    break  # break if we don't yet have enough data to consume the whole message

                if typ == MessageType.ERROR and length == ERROR_MESSAGE_SIZE:
                    client_order_id, error_message = ERROR_MESSAGE.unpack_from(
                        self._data, upto + HEADER_SIZE
                    )
                    self.on_error_message(
                        client_order_id, error_message.rstrip(b"\x00").decode("utf-8")
                    )

                elif (
                    typ == MessageType.ORDER_STATUS and length == ORDER_STATUS_MESSAGE_SIZE
                ):
                    self.on_order_status_message(
                        *ORDER_STATUS_MESSAGE.unpack_from(
                            self._data, upto + HEADER_SIZE
                        )
                    )

                elif (
                    typ == MessageType.POSITION_CHANGE and length == POSITION_CHANGE_MESSAGE_SIZE
                ):
                    self.on_position_change_message(
                        *POSITION_CHANGE_MESSAGE.unpack_from(
                            self._data, upto + HEADER_SIZE
                        )
                    )

                elif (
                    typ == MessageType.CLIENTSTATE and length == CLIENTSTATE_MESSAGE_SIZE
                ):
                    self.on_clientstate_message(
                        *CLIENTSTATE_MESSAGE.unpack_from(self._data, upto + HEADER_SIZE)
                    )

                elif (
                    typ == MessageType.ORDER_BOOK_UPDATE and length == ORDER_BOOK_MESSAGE_SIZE
                ):
                    inst, seq = ORDER_BOOK_HEADER.unpack_from(
                        self._data, upto + HEADER_SIZE
                    )
                    self.on_order_book_update_message(
                        inst,
                        seq,
                        *self.BOOK_PART.iter_unpack(
                            data[upto + ORDER_BOOK_HEADER_SIZE: upto + ORDER_BOOK_MESSAGE_SIZE]
                        )
                    )

                elif (
                    typ == MessageType.TRADE_TICKS and (
                        length - TRADE_TICKS_HEADER_SIZE) % TRADE_TICK_SIZE == 0
                ):
                    (inst,) = TRADE_TICKS_HEADER.unpack_from(
                        self._data, upto + HEADER_SIZE
                    )
                    ticks_nbytes = length - TRADE_TICKS_HEADER_SIZE
                    if ticks_nbytes % TRADE_TICK_SIZE != 0:
                        self.logger.error(
                            "received TRADE_TICKS with unexpected padding. len=%d ticks_nbytes=%d",
                            length,
                            ticks_nbytes,
                        )
                        return self.exit_client(ExitCode.CLIENT_COMM_ERROR)
                    ticks = list(
                        TRADE_TICK.iter_unpack(
                            data[upto + TRADE_TICKS_HEADER_SIZE: upto + length]
                        )
                    )
                    self.on_trade_ticks_message(inst, ticks)

                elif (
                    typ == MessageType.NEW_PARTICIPANT and length == NEW_PARTICIPANT_MESSAGE_SIZE
                ):
                    pid, name = NEW_PARTICIPANT_MESSAGE.unpack_from(
                        self._data, upto + HEADER_SIZE
                    )
                    self.on_new_participant(pid, name.rstrip(b"\x00").decode("utf-8"))

                elif typ == MessageType.MARKET_PNL:
                    num_items = PNL_DATA_HEADER.unpack_from(
                        self._data, upto + HEADER_SIZE
                    )
                    pnls = list(
                        PNL_DATA.iter_unpack(
                            data[upto + PNL_DATA_HEADER_SIZE: upto + length]
                        )
                    )
                    self.on_pnl(pnls)

                else:
                    self.logger.error(
                        "received invalid execution message: length=%d type=%d",
                        length,
                        typ,
                    )
                    return self.exit_client(ExitCode.CLIENT_COMM_ERROR)

                upto += length

        except BaseException as ex:
            handle_exception(ex, "receiving data")
            # sys.exit(exit_code) # Window's top-right [x] button
            # on_exit_app(1)
            raise ex

        self._data = self._data[upto:]
