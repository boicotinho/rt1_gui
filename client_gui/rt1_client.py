#!/opt/rh/rh-python36/root/usr/bin/python3
import sys
import os
import traceback
import pprint
import socket
import asyncio
import logging
import struct
import random
import enum
import math
import numpy
from functools import reduce
from collections import deque
from typing import List, Optional, Text, Tuple, Union
from asyncqt import QEventLoop, asyncSlot, asyncClose

from PyQt5.uic import loadUiType
from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (
    QWidget,
    QTableWidget,
    QTableWidgetItem,
    QVBoxLayout,
    QMdiSubWindow,
    qApp,
)
from cltstate import *

from pyqtgraph import PlotWidget, plot
import pyqtgraph

# Import window and gadget definitions
Ui_MainWindow, QMainWindow = loadUiType("main_window.ui")
Ui_LoginWindow, QLoginWindow = loadUiType("login_window.ui")
Ui_PxLadderGadget1, QPxLadderGadget1 = loadUiType("price_ladder.ui")
Ui_PxLadderGadget2, QPxLadderGadget2 = loadUiType("price_ladder2.ui")
Ui_TimeSeriesGadget, QTimeSeriesGadget = loadUiType("time_series.ui")
Ui_OrderViewGadget, QOrderViewGadget = loadUiType("order_view.ui")
Ui_TradeViewGadget, QTradeViewGadget = loadUiType("trade_view.ui")
Ui_PnlGadget, QPnlGadget = loadUiType("pnl_table.ui")

def color_scaled(r, g, b, x: float = 1.0):
    return QtGui.QColor(r * x, g * x, b * x)

class Theme:
    COLOR_OPTIVER = QtGui.QColor(232, 117, 90)
    PWIDTH = 2.0
    TRADES_LOOKBACK = 30
    DEFAULT_LOOKBACK = 240
    DEFAULT_PLOT_DELAY_S = 1.0
    USE_OLD_PX_LADDER = False
    COLOR_ETF = QtGui.QColor(61, 174, 233)
    COLOR_FUT = COLOR_OPTIVER
    COLOR_BK_CXL = COLOR_OPTIVER
    COLOR_BK_MPX = QtGui.QColor(35, 38, 41)
    COLOR_BK_PX = QtGui.QColor(35, 38, 41)
    COLOR_FG_ASK = COLOR_OPTIVER
    COLOR_FG_BID = QtGui.QColor(61, 174, 233)
    COLOR_BK_QX = color_scaled(25, 35, 45, 1.2)
    COLOR_FG_QX = color_scaled(240, 240, 240)
    TABLE_FONT = QtGui.QFont("Sans Serif", 7)
    INSTR_NAME = ["Futures", "ETF"]
    STR_SIDE = ["Sell", "Buy"]
    ICON: QtGui.QIcon = None

    def __init__(self, a_cfg):
        self.qss = open(
            "default.qss", "r"
        ).read()  # https://doc.qt.io/qt-5/stylesheet-reference.html
        self.qss += "\nQMdiSubWindow{ border: 8px solid #32414B; }"
        Theme.ICON = QtGui.QIcon(os.path.join(this_script_dir, "logo_dark.png"))
        Theme.PWIDTH = a_cfg.get("PlotWidth", Theme.PWIDTH)
        Theme.TRADES_LOOKBACK = a_cfg.get("NumTrades", Theme.TRADES_LOOKBACK)
        Theme.DEFAULT_LOOKBACK = a_cfg.get("Lookback", Theme.DEFAULT_LOOKBACK)
        Theme.DEFAULT_PLOT_DELAY_S = a_cfg.get("PlotDelay", Theme.DEFAULT_PLOT_DELAY_S)
        Theme.USE_OLD_PX_LADDER = a_cfg.get("UseOldPriceLadder", Theme.USE_OLD_PX_LADDER)
        pyqtgraph.setConfigOption("background", Theme.COLOR_BK_QX)


class Configuration:
    FILENAME_CFG: str = None
    g_cfg: Dict[str, Any] = None
    def __init__(self, a_config_file_path):
        Configuration.FILENAME_CFG = a_config_file_path
        Configuration.g_cfg = load_config(a_config_file_path)
        Configuration.theme = Theme(Configuration.g_cfg)
    def save():
        try:
            if Configuration.g_cfg is not None:
                save_config(Configuration.FILENAME_CFG, Configuration.g_cfg)
        except BaseException:
            pass


def on_exit_app(exit_code: int):
    Configuration.save()
    sys.exit(exit_code)  # Window's top-right [x] button


@tperf
def window_config_map(wnd, name, save: bool) -> None:
    if save:
        window_config_map_save(wnd, name)
    else:
        return window_config_map_load(wnd, name)


@tperf
def window_config_map_load(wnd, name) -> bool:
    #return False
    if Theme.ICON is not None:
        wnd.setWindowIcon(Theme.ICON)
    if name not in Configuration.g_cfg:
        return False
    entry = Configuration.g_cfg[name]
    if "Width" in entry and "Height" in entry:
        cx = entry["Width"]
        cy = entry["Height"]
        if "X" in entry and "Y" in entry:
            x = entry["X"]
            y = entry["Y"]
            wnd.setGeometry(x, y, cx, cy)
            return True
        else:
            wnd.resize(cx, cy)
            return True
    return False


@tperf
def window_config_map_save(wnd, name, show=None) -> None:
    if name not in Configuration.g_cfg:
        Configuration.g_cfg[name] = {}
    rc: QtCore.QRect = wnd.geometry()
    entry = Configuration.g_cfg[name]
    entry["X"] = rc.left()
    entry["Y"] = rc.top()
    entry["Width"] = rc.width()
    entry["Height"] = rc.height()
    if show is not None:
        entry["Show"] = bool(show)


@tperf
def window_config_map_init_visible(name) -> bool:
    if name not in Configuration.g_cfg:
        return True
    entry = Configuration.g_cfg[name]
    return ("Show" not in entry) or bool(entry["Show"]) is True


class MainWindow(Ui_MainWindow, QMainWindow, IMarketEventListenner):
    g_main_wnd = None
    def __init__(self):
        g_main_wnd = self
        super().__init__()
        self.setupUi(self)
        self.setWindowTitle("Ready Trader One Client")
        self.logger = logging.getLogger("MAIN")
        window_config_map_load(self, "WindowMain")

        self.action_Quit.setShortcut("Ctrl+Q")
        self.action_Quit.setStatusTip("Exit application")
        self.action_Quit.triggered.connect(qApp.quit)

        self.action_pxladder_etf.triggered.connect(lambda: self.mdi_pxladder(1))
        self.action_plot_etf.triggered.connect(lambda: self.mdi_plot([1]))
        self.action_plot_fut.triggered.connect(lambda: self.mdi_plot([0]))
        self.action_order_view.triggered.connect(self.mdi_order_view)
        self.action_team_compare.triggered.connect(self.mdi_team_compare)
        self.action_plot_spread.triggered.connect(self.mdi_plot_spread)
        self.action_trades.triggered.connect(self.mdi_trades_view)
        self.action_plot_pnl.triggered.connect(self.mdi_plot_pnl)

        self.windows = []

        if window_config_map_init_visible("PriceLadderETF"):
            self.mdi_pxladder()

        if window_config_map_init_visible("TimeSeriesETF"):
            self.mdi_plot([1])

        if window_config_map_init_visible("TimeSeriesFutures"):
            self.mdi_plot([0])

        if window_config_map_init_visible("OrderView"):
            self.mdi_order_view()

        if window_config_map_init_visible("TeamCompare"):
            self.mdi_team_compare()

        if window_config_map_init_visible("TimeSeriesSpread"):
            self.mdi_plot_spread()

        if window_config_map_init_visible("TradeView"):
            self.mdi_trades_view()

        if window_config_map_init_visible("ProfitOverTime"):
            self.mdi_plot_pnl()

        self.action_tile_windows_2.triggered.connect(self.mdiArea.tileSubWindows)

    @tperf
    def closeEvent(self, event):
        window_config_map_save(self, "WindowMain")

    @tperf
    def on_close_mdi_window(wnd, name):
        g_main_wnd.window_config_map_save(wnd, name, False)
        g_main_wnd.windows.remove(wnd)

    def mdi_plot_pnl(self) -> None:
        sub = PnlOverTimeGadget()
        self.mdiArea.addSubWindow(sub)
        sub.show()
        self.windows.append(sub)
        sub.load_wnd_map()

    def mdi_plot_spread(self) -> None:
        sub = SpreadGadget()
        self.mdiArea.addSubWindow(sub)
        sub.show()
        self.windows.append(sub)
        sub.load_wnd_map()

    def mdi_trades_view(self) -> None:
        sub = TradeViewGadget()
        self.mdiArea.addSubWindow(sub)
        sub.show()
        self.windows.append(sub)
        sub.load_wnd_map()

    def mdi_pxladder(self) -> None:
        Gagdet = PxLadderGadget if Theme.USE_OLD_PX_LADDER else PriceLadderGadget
        for wnd in self.windows:
            if isinstance(wnd, Gagdet):
                return
        sub = Gagdet(PriceLadderModel(100))
        self.mdiArea.addSubWindow(sub)
        sub.show()
        self.windows.append(sub)
        sub.load_wnd_map()

    def mdi_plot(self, instruments) -> None:
        sub = TimeSeriesGadget(instruments)
        self.mdiArea.addSubWindow(sub)
        sub.show()
        self.windows.append(sub)
        sub.load_wnd_map()

    def mdi_order_view(self) -> None:
        sub = OrderViewGadget()
        self.mdiArea.addSubWindow(sub)
        sub.show()
        self.windows.append(sub)
        sub.load_wnd_map()

    def mdi_team_compare(self) -> None:
        sub = PnlGadget()
        self.mdiArea.addSubWindow(sub)
        sub.show()
        self.windows.append(sub)
        sub.load_wnd_map()

    def on_error_message(self, client_order_id: int, error_message: bytes) -> None:
        errwnd = QtWidgets.QMessageBox(self)
        errwnd.setIcon(QtWidgets.QMessageBox.Critical)
        errwnd.setText("OrderID [%d] Error:" % (client_order_id))
        errwnd.setInformativeText(error_message)
        errwnd.setWindowTitle("Error")
        errwnd.show()

    def on_order_book_update_message(self, book: Book) -> None:
        for wnd in self.windows:
            wnd.on_order_book_update_message(book)

    def on_order_status_message(
        self, oid: int, fill: int, remaining: int, fees: int,
        side, lifespan, org_price, fill_price
    ) -> None:
        for wnd in self.windows:
            wnd.on_order_status_message(oid)


class LoginWindow(Ui_LoginWindow, QLoginWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.logger = logging.getLogger("LOGIN")

        self.server = Configuration.g_cfg["Execution"]["Host"]  # '127.0.0.1'
        self.port = Configuration.g_cfg["Execution"]["Port"]  # 12345
        self.username = Configuration.g_cfg["TeamName"]  # 'MrClick'
        self.password = Configuration.g_cfg["Secret"]  # 'click'

        self.edit_hostnameport.setPlainText("%s:%s" % (self.server, self.port))
        self.edit_teamname.setPlainText(self.username)
        self.edit_password.setPlainText(self.password)

    @tperf
    def get_ui_vars(self):
        self.username = self.edit_teamname.toPlainText()
        self.password = self.edit_password.toPlainText()
        hostport = self.edit_hostnameport.toPlainText().split(":")
        self.server = hostport[0]
        self.port = int(hostport[1])

        Configuration.g_cfg["Execution"]["Host"] = self.server
        Configuration.g_cfg["Execution"]["Port"] = int(self.port)
        Configuration.g_cfg["TeamName"] = self.username
        Configuration.g_cfg["Secret"] = self.password

    @tperf
    def accept(self):
        self.get_ui_vars()
        self.logger.info(
            "Attempting login: %s:%d, '%s', '%s'"
            % (self.server, self.port, self.username, self.password)
        )
        super().accept()  # closes and return from window.exec_()


class PxLadderGadget(Ui_PxLadderGadget1, QPxLadderGadget1):
    def __init__(self, model):
        self.closing = False
        self.settings_loaded = False
        super().__init__()
        self.setupUi(self)
        self.logger = logging.getLogger("PXLADDER")
        self.pxLadderTable.viewport().installEventFilter(self)
        self.curr_firstpx = 0
        self.table_midpx = 0
        self.instrument = 1
        self.book = None
        self.btCxlAll.clicked.connect(lambda: g_om.cancel_all(1, True, True, 0))
        self.btCxlBids.clicked.connect(lambda: g_om.cancel_all(1, False, True, 0))
        self.btCxlAsks.clicked.connect(lambda: g_om.cancel_all(1, True, False, 0))
        self.btRecenter.clicked.connect(lambda: self.recenter())
        self.installEventFilter(self)

    def closeEvent(self, event):
        self.closing = True
        on_close_mdi_window(self, "PriceLadderETF")

    def load_wnd_map(self):
        window_config_map_load(self.parent(), "PriceLadderETF")
        self.settings_loaded = True

    def event(self, ev):
        response = super().event(ev)
        if ev.type() == QtCore.QEvent.Resize or int(ev.type()) == 12:
            if not self.closing and self.parent() is not None and self.settings_loaded:
                window_config_map_save(self.parent(), "PriceLadderETF", True)
        return response

    def on_order_status_message(self, oid):
        pass

    def recenter(self):
        self.table_midpx = int(self.book.midpx())

    def resizeEvent(self, evt):
        # print('resizeEvent : %s -> %s' % (evt.oldSize(), evt.size()))
        estimate = evt.size().height() / 21
        vh = self.pxLadderTable.verticalHeader().height()
        hh = self.pxLadderTable.horizontalHeader().height()  # 21
        num_rows = vh // hh
        if num_rows < 10:
            num_rows = estimate
        if num_rows > 100:
            num_rows = estimate
        self.pxLadderTable.setRowCount(num_rows)
        # print(dir(self.pxLadderTable.verticalHeader()))

    def on_order_book_update_message(self, book: Book) -> None:
        if book.instrument != self.instrument:
            return
        self.book = book
        self.ob_pxl_update()

    @tperf
    def ob_pxl_update(self, dummy):

        true_mpx = int(self.book.midpx())

        if self.table_midpx == 0:
            self.table_midpx = true_mpx
        mpx = self.table_midpx

        R = self.pxLadderTable.rowCount()  # 30
        C = self.pxLadderTable.columnCount()  # 5

        # Prices
        hipx = mpx + 100 * math.floor((R / 2) - 1)
        self.curr_firstpx = hipx
        mpx_row = -1
        for rr in range(R):
            px = int(hipx - rr * 100)
            is_ask = px >= true_mpx

            self.pxLadderTable.setItem(
                rr, 2, QTableWidgetItem("%.2f" % (px / 100))
            )  # price
            px_cell = self.pxLadderTable.item(rr, 2)
            px_cell.setForeground(Theme.COLOR_FG_ASK if is_ask else Theme.COLOR_FG_BID)
            px_cell.setBackground(Theme.COLOR_BK_PX)
            if px == true_mpx:
                mpx_row = rr
            if is_ask:  # ask
                self.pxLadderTable.setItem(
                    rr, 4, QTableWidgetItem(g_om.qty_str_at_px(Side.SELL, px))
                )
                self.pxLadderTable.setItem(rr, 1, QTableWidgetItem(""))
                sqx = "%d" % (self.book.a_dict[px]) if px in self.book.a_dict else ""
                self.pxLadderTable.setItem(rr, 3, QTableWidgetItem(sqx))
            else:  # bid
                self.pxLadderTable.setItem(
                    rr, 0, QTableWidgetItem(g_om.qty_str_at_px(Side.BUY, px))
                )
                self.pxLadderTable.setItem(rr, 3, QTableWidgetItem(""))
                sqx = "%d" % (self.book.b_dict[px]) if px in self.book.b_dict else ""
                self.pxLadderTable.setItem(rr, 1, QTableWidgetItem(sqx))
        # Set colors
        for rr in range(R):
            for cc in range(C):
                if cc == 2:
                    continue
                cell = self.pxLadderTable.item(rr, cc)
                if cell is None:
                    self.pxLadderTable.setItem(rr, cc, QTableWidgetItem(""))
                    cell = self.pxLadderTable.item(rr, cc)
                if cell is not None:
                    cell.setBackground(Theme.COLOR_BK_QX)
                    cell.setForeground(Theme.COLOR_FG_QX)
        if mpx_row >= 0:
            for cc in range(C):
                cell = self.pxLadderTable.item(mpx_row, cc)
                if cell is None:
                    self.pxLadderTable.setItem(mpx_row, cc, QTableWidgetItem(""))
                    cell = self.pxLadderTable.item(mpx_row, cc)
                cell.setBackground(Theme.COLOR_BK_MPX)

    @tperf
    def eventFilter(self, source, event):
        is_click = event.type() == QtCore.QEvent.MouseButtonPress
        if (
            is_click and self.book is not None and source is self.pxLadderTable.viewport()
        ):
            if event.buttons() == QtCore.Qt.LeftButton:
                bt = "L"
            elif event.buttons() == QtCore.Qt.RightButton:
                bt = "R"
            else:
                bt = "?"
                return super().eventFilter(source, event)
            item = self.pxLadderTable.itemAt(event.pos())
            if item is not None:
                if item.column() == 2:
                    self.recenter()
                px = self.curr_firstpx - self.book.tick_size * item.row()
                qx = self.clickQty.value()
                if item.column() in [0]:
                    side = Side.BUY
                elif item.column() in [4]:
                    side = Side.SELL
                else:
                    return super().eventFilter(source, event)
                if bt == "L":
                    self.logger.info("Order enter %s %d @ px=%s" % (side, qx, px))
                    g_om.new_order(self.book.instrument, side, px, qx)
                else:
                    self.logger.info("Order cancel %s %d @ px=%s" % (side, qx, px))
                    g_om.cancel_at_px(self.book.instrument, px)
        return super().eventFilter(source, event)


class PriceLadderModel(QtCore.QAbstractTableModel):
    ASK_VOL_COLUMN = 3
    BID_VOL_COLUMN = 1
    COLUMN_NAMES = ("MyBid", "BidVol", "Price", "AskVol", "MyAsk")
    PRICE_COLUMN = 2
    MY_ASK_COLUMN = 4
    MY_BID_COLUMN = 0

    best_ask_row_changed: QtCore.pyqtSignal = QtCore.pyqtSignal(int)

    def __init__(self, tick_size: int):
        super().__init__()

        self.ask_volumes: Dict[int, QtCore.QVariant] = dict()
        self.bid_volumes: Dict[int, QtCore.QVariant] = dict()
        self.column_names: List[QtCore.QVariant] = list(
            map(QtCore.QVariant, self.COLUMN_NAMES)
        )
        self.first_price: int = 0
        self.invalid_variant: QtCore.QVariant = QtCore.QVariant()
        self.last_best_ask: int = 0
        self.row_count: int = 0
        self.tick_size: int = tick_size
        self.visual_ask: int = 0
        self.visual_bid: int = 0

    def columnCount(self, parent: QtCore.QModelIndex) -> int:
        return 0 if parent.isValid() else len(self.column_names)

    def row_2_price(self, row: int):
        price = self.first_price - row * self.tick_size  # index.row()
        return price

    @tperf
    def data(self, index: QtCore.QModelIndex, role: Qt.ItemDataRole) -> QtCore.QVariant:
        if (
            role == Qt.DisplayRole
        ):  # https://www.riverbankcomputing.com/static/Docs/PyQt4/qt.html#ItemDataRole-enum
            price = self.first_price - index.row() * self.tick_size
            column: int = index.column()
            if column == self.BID_VOL_COLUMN:
                return self.bid_volumes.get(price, self.invalid_variant)
            elif column == self.PRICE_COLUMN:
                return QtCore.QVariant("%.2f" % (price / self.tick_size))
            elif column == self.ASK_VOL_COLUMN:
                return self.ask_volumes.get(price, self.invalid_variant)
            elif column == self.MY_BID_COLUMN:
                ss = g_om.qty_str_at_px(Side.BUY, price)
                return QtCore.QVariant(ss) if len(ss) > 0 else self.invalid_variant
            elif column == self.MY_ASK_COLUMN:
                ss = g_om.qty_str_at_px(Side.SELL, price)
                return QtCore.QVariant(ss) if len(ss) > 0 else self.invalid_variant
        elif role == Qt.TextAlignmentRole:
            return Qt.AlignCenter
        elif role == Qt.DecorationRole:  # 1
            pass
        elif role == Qt.ToolTipRole:  # 3
            pass
        elif role == Qt.StatusTipRole:  # 4
            pass
        elif role == Qt.FontRole:  # 6
            return QtCore.QVariant(Theme.TABLE_FONT)
        elif role == Qt.BackgroundRole:  # 8
            column: int = index.column()
            return QtCore.QVariant(
                Theme.COLOR_BK_PX if column == self.PRICE_COLUMN else Theme.COLOR_BK_QX
            )
        elif role == Qt.ForegroundRole:  # 9
            price = self.first_price - index.row() * self.tick_size
            column: int = index.column()
            if column != self.PRICE_COLUMN:
                return QtCore.QVariant(Theme.COLOR_FG_QX)
            if price >= self.visual_ask:
                color = Theme.COLOR_FG_ASK
            elif price <= self.visual_bid:
                color = Theme.COLOR_FG_BID
            else:
                color = Theme.COLOR_FG_QX
            return QtCore.QVariant(color)
        elif role == Qt.CheckStateRole:  # 10
            pass
        else:
            print(role)
        return self.invalid_variant

    @tperf
    def headerData(
        self, section: int, orientation: Qt.Orientation, role: Qt.ItemDataRole
    ) -> QtCore.QVariant:
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.column_names[section]
        return self.invalid_variant

    def rowCount(self, parent: QtCore.QModelIndex) -> int:
        return 0 if parent.isValid() else self.row_count

    @tperf
    def append_rows(self, count: int) -> None:
        self.beginInsertRows(
            QtCore.QModelIndex(), self.row_count, self.row_count + count - 1
        )
        self.row_count += count
        self.endInsertRows()

    @tperf
    def prepend_rows(self, count: int) -> None:
        self.beginInsertRows(QtCore.QModelIndex(), 1, count)
        self.row_count += count
        self.first_price += count * self.tick_size
        self.endInsertRows()

    @tperf
    def update_order_book(
        self,
        ask_prices: List[int],
        ask_volumes: List[int],
        bid_prices: List[int],
        bid_volumes: List[int],
    ) -> None:
        self.ask_volumes.clear()
        self.bid_volumes.clear()

        self.visual_ask = ask_prices[0]
        self.visual_bid = bid_prices[0]

        best_ask: int = ask_prices[0]
        if best_ask == 0 and bid_prices[0] != 0:
            best_ask = bid_prices[0] + self.tick_size

        if self.row_count == 0:
            if best_ask == 0:
                return

            # Create the initial table rows if this is the first (valid) order book
            worst_ask: int = next((p for p in reversed(ask_prices) if p), best_ask)
            self.first_price = worst_ask

            best_bid: int = (
                bid_prices[0] if bid_prices[0] else best_ask - self.tick_size
            )
            worst_bid: int = next((p for p in reversed(bid_prices) if p), best_bid)

            self.append_rows((worst_ask - worst_bid) // self.tick_size + 1)

        self.ask_volumes.update(
            (p, QtCore.QVariant("{:,}".format(v)))
            for p, v in zip(ask_prices, ask_volumes)
            if p
        )
        self.dataChanged.emit(
            self.createIndex(0, self.ASK_VOL_COLUMN),
            self.createIndex(self.row_count - 1, self.ASK_VOL_COLUMN),
        )
        self.dataChanged.emit(
            self.createIndex(0, self.MY_ASK_COLUMN),
            self.createIndex(self.row_count - 1, self.MY_ASK_COLUMN),
        )

        self.bid_volumes.update(
            (p, QtCore.QVariant("{:,}".format(v)))
            for p, v in zip(bid_prices, bid_volumes)
            if p
        )
        self.dataChanged.emit(
            self.createIndex(0, self.BID_VOL_COLUMN),
            self.createIndex(self.row_count - 1, self.BID_VOL_COLUMN),
        )
        self.dataChanged.emit(
            self.createIndex(0, self.MY_BID_COLUMN),
            self.createIndex(self.row_count - 1, self.MY_BID_COLUMN),
        )

        if best_ask and best_ask != self.last_best_ask:
            self.best_ask_row_changed.emit(
                (self.first_price - best_ask) // self.tick_size
            )
            self.last_best_ask = best_ask


class LadderEventFilter(QtCore.QObject):
    """Event filter for the price ladder."""

    def __init__(self, ladder_gadget: QtCore.QObject):
        super().__init__(ladder_gadget)
        self.ladder_gadget: QtCore.QObject = ladder_gadget

    @tperf
    def eventFilter(self, source: QtCore.QObject, event: QtCore.QEvent):
        typ = event.type()
        if typ == QtCore.QEvent.MouseButtonPress:
            if event.buttons() == QtCore.Qt.LeftButton:
                bt = "L"
            elif event.buttons() == QtCore.Qt.RightButton:
                bt = "R"
            else:
                return super().eventFilter(source, event)
            row = self.ladder_gadget.pxLadderTable.rowAt(event.pos().y())
            col = self.ladder_gadget.pxLadderTable.columnAt(event.pos().x())
            px = self.ladder_gadget.table_model.row_2_price(row)
            # print('col=%d, row=%d / px=%d' %(col,row,px))
            if col >= 0 and row >= 0:
                qx = self.ladder_gadget.clickQty.value()
                if col == 0:
                    side = Side.BUY
                elif col == 4:
                    side = Side.SELL
                else:
                    return super().eventFilter(source, event)
                if bt == "L":
                    self.ladder_gadget.logger.info(
                        "Order enter %s %d @ px=%s" % (side, qx, px)
                    )
                    g_om.new_order(1, side, px, qx)
                else:
                    self.ladder_gadget.logger.info(
                        "Order cancel %s %d @ px=%s" % (side, qx, px)
                    )
                    g_om.cancel_at_px(1, px)
            return True
        elif typ in (QtCore.QEvent.MouseButtonRelease, QtCore.QEvent.MouseMove):
            return True
        elif typ == QtCore.QEvent.Wheel:  # ignore mouse wheel events
            return True
        elif typ == QtCore.QEvent.Resize:
            if self.ladder_gadget.pxLadderTable.model().rowCount(QtCore.QModelIndex()):
                self.ladder_gadget.recenter_best_ask_row()
            return False
        return super().eventFilter(source, event)


class PriceLadderGadget(Ui_PxLadderGadget2, QPxLadderGadget2):
    def __init__(self, table_model: PriceLadderModel):
        self.closing = False
        self.settings_loaded = False
        self.logger = logging.getLogger("PXLADDER")
        super().__init__()

        self.best_ask_row: int = 0
        self.last_best_ask: int = 10000
        self.table_model: PriceLadderModel = table_model
        self.table_model.best_ask_row_changed.connect(self.best_ask_row_changed)

        self.setupUi(self)
        self.setWindowTitle("ETF - Dynamic Depth Window")

        self.pxLadderTable.horizontalHeader().setSectionResizeMode(
            QtWidgets.QHeaderView.Stretch
        )
        self.pxLadderTable.setModel(table_model)
        self.pxLadderTable.viewport().installEventFilter(LadderEventFilter(self))

        self.animation = QtCore.QVariantAnimation()
        self.animation.valueChanged.connect(
            self.pxLadderTable.verticalScrollBar().setValue
        )
        self.animation.finished.connect(
            lambda: self.pxLadderTable.selectRow(self.best_ask_row)
        )

        self.btCxlAll.clicked.connect(self.delete_all)
        self.btCxlBids.clicked.connect(self.delete_bids)
        self.btCxlAsks.clicked.connect(self.delete_asks)

    def closeEvent(self, event):
        self.closing = True
        on_close_mdi_window(self, "PriceLadderETF")

    def load_wnd_map(self):
        window_config_map_load(self.parent(), "PriceLadderETF")
        self.settings_loaded = True

    def event(self, ev):
        response = super().event(ev)
        if ev.type() == QtCore.QEvent.Resize or int(ev.type()) == 12:
            if not self.closing and self.parent() is not None and self.settings_loaded:
                window_config_map_save(self.parent(), "PriceLadderETF", True)
        return response

    def on_order_status_message(self, oid):
        pass

    def on_order_book_update_message(self, book: Book) -> None:
        if book.instrument != 1:
            return
        self.table_model.update_order_book(
            book.ask_prices, book.ask_volumes, book.bid_prices, book.bid_volumes
        )

    def best_ask_row_changed(self, new_best_ask_row: int):
        """Callback when the best ask row changes."""
        # if abs(new_best_ask_row - self.best_ask_row) < 5:
        #    return
        self.best_ask_row = new_best_ask_row
        self.recenter_best_ask_row()

    def delete_all(self):
        g_om.cancel_all(1, True, True, 0)

    def delete_bids(self):
        g_om.cancel_all(1, False, True, 0)

    def delete_asks(self):
        g_om.cancel_all(1, True, False, 0)

    @tperf
    def recenter_best_ask_row(self):
        self.animation.stop()

        row_height: int = self.pxLadderTable.verticalHeader().defaultSectionSize()
        half_viewport_height: int = self.pxLadderTable.viewport().height() // 2

        row_count: int = self.table_model.rowCount(QtCore.QModelIndex())
        bids_height: int = (row_count - self.best_ask_row - 1) * row_height
        append_rows: bool = bids_height < half_viewport_height
        if append_rows:
            self.table_model.append_rows(
                (half_viewport_height - bids_height) // row_height + 1
            )

        asks_height: int = (self.best_ask_row + 1) * row_height
        scroll_from: int = self.pxLadderTable.verticalScrollBar().value()
        scroll_to: int = asks_height - half_viewport_height
        if asks_height < half_viewport_height:
            additional_row_count: int = (
                half_viewport_height - asks_height
            ) // row_height + 1
            self.table_model.prepend_rows(additional_row_count)
            self.best_ask_row += additional_row_count
            if not append_rows:
                # if we didn't append rows then adjust scroll_from
                scroll_from += additional_row_count * row_height
            scroll_to += additional_row_count * row_height

        if scroll_from != scroll_to:
            self.animation.setStartValue(scroll_from)
            self.animation.setEndValue(scroll_to)
            duration: int = round(abs(scroll_from - scroll_to) / row_height * 250)
            self.animation.setDuration(
                750 if duration > 750 else (400 if duration < 400 else duration)
            )
            self.animation.start()


class TimeSeriesGadget(Ui_TimeSeriesGadget, QTimeSeriesGadget):
    def __init__(self, instruments: List[int]):
        self.config_name = None
        self.settings_loaded = False
        super().__init__()
        self.setupUi(self)
        self.graphWidget = pyqtgraph.PlotWidget(self)
        self.graphWidget.showGrid(True, True)
        self.verticalLayout_2.addWidget(self.graphWidget)
        self.spinBox.setRange(10, 1000000)
        self.spinBox.setSingleStep(10)
        self.spinBox.setValue(Theme.DEFAULT_LOOKBACK)
        self.btAutoScroll.clicked.connect(
            lambda: self.graphWidget.enableAutoRange(
                axis=None, enable=True, x=None, y=None
            )
        )

        self.instruments = instruments
        self.ts_next_plot = 0
        self.books = {}
        instr_name = ", ".join([Theme.INSTR_NAME[ii] for ii in instruments])
        self.setWindowTitle(instr_name)
        self.graphWidget.setTitle("Midprice: " + instr_name)
        self.plot_pens = {
            0: pyqtgraph.mkPen(color=Theme.COLOR_FUT, width=Theme.PWIDTH),  # FUT: blue
            1: pyqtgraph.mkPen(color=Theme.COLOR_ETF, width=Theme.PWIDTH),  # ETF: red
        }
        self.config_name = "TimeSeries" + "+".join(
            [Theme.INSTR_NAME[ii] for ii in instruments]
        )
        window_config_map_load(self, self.config_name)

    def closeEvent(self, event):
        on_close_mdi_window(self, self.config_name)
        self.config_name = None

    def load_wnd_map(self):
        window_config_map_load(self.parent(), self.config_name)
        self.settings_loaded = True

    def event(self, ev):
        response = super().event(ev)
        if ev.type() == QtCore.QEvent.Resize or int(ev.type()) == 12:
            if self.config_name is not None and self.parent() is not None and self.settings_loaded:
                window_config_map_save(self.parent(), self.config_name, True)
        return response

    def on_order_book_update_message(self, latest_book: Book) -> None:
        if latest_book.instrument not in self.instruments:
            return
        if time.time() < self.ts_next_plot:
            return
        self.replot_px(latest_book)
        self.ts_next_plot = time.time() + Theme.DEFAULT_PLOT_DELAY_S

    @tperf
    def replot_px(self, latest_book):
        self.books[latest_book.instrument] = latest_book
        look_back = self.spinBox.value()
        self.graphWidget.clear()
        for ii in self.instruments:
            if ii not in self.books:
                continue
            book = self.books[ii]
            seqnos = book.tplot_x_seqno[-look_back:]
            prices = book.tplot_y_mid_px[-look_back:]
            ppen = self.plot_pens[ii]
            self.graphWidget.plot(seqnos, prices, pen=ppen)

    def on_order_status_message(self, oid):
        pass


class SpreadGadget(Ui_TimeSeriesGadget, QTimeSeriesGadget):
    def __init__(self):
        self.config_name = None
        self.settings_loaded = False
        super().__init__()
        self.setupUi(self)
        self.ts_next_plot = 0
        self.graphWidget = pyqtgraph.PlotWidget(self)
        self.graphWidget.showGrid(True, True)
        self.plot_pens = {
            0: pyqtgraph.mkPen(color=Theme.COLOR_FUT, width=Theme.PWIDTH),
            1: pyqtgraph.mkPen(color=Theme.COLOR_ETF, width=Theme.PWIDTH),
        }
        plt_item = self.graphWidget.plotItem
        self.plt_vbox = pyqtgraph.ViewBox()
        plt_item.showAxis("right")
        plt_item.scene().addItem(self.plt_vbox)
        plt_item.getAxis("right").linkToView(self.plt_vbox)
        self.plt_vbox.setXLink(plt_item)
        plt_item.getAxis("left").setLabel("Price Spread: FUT-ETF")  # COLOR_ETF
        plt_item.getAxis("left").setTextPen(self.plot_pens[0])
        plt_item.getAxis("right").setLabel("ETF Position")
        plt_item.getAxis("right").setTextPen(self.plot_pens[1])
        self.updateViews()
        plt_item.vb.sigResized.connect(self.updateViews)
        self.verticalLayout_2.addWidget(self.graphWidget)
        self.spinBox.setRange(10, 1000000)
        self.spinBox.setSingleStep(10)
        self.spinBox.setValue(Theme.DEFAULT_LOOKBACK)
        self.btAutoScroll.clicked.connect(
            lambda: self.graphWidget.enableAutoRange(
                axis=None, enable=True, x=None, y=None
            )
        )
        self.books = {}
        self.setWindowTitle("Spread")
        self.graphWidget.setTitle("Spread & ETF Pos")
        self.config_name = "TimeSeriesSpread"
        window_config_map_load(self, self.config_name)

    @tperf
    def updateViews(self, dummy=1):
        plt_item = self.graphWidget.plotItem
        self.plt_vbox.setGeometry(plt_item.vb.sceneBoundingRect())
        self.plt_vbox.linkedViewChanged(plt_item.vb, self.plt_vbox.XAxis)

    def closeEvent(self, event):
        on_close_mdi_window(self, self.config_name)
        self.config_name = None

    def load_wnd_map(self):
        window_config_map_load(self.parent(), self.config_name)
        self.settings_loaded = True

    def event(self, ev):
        response = super().event(ev)
        if ev.type() == QtCore.QEvent.Resize or int(ev.type()) == 12:
            if self.config_name is not None and self.parent() is not None and self.settings_loaded:
                window_config_map_save(self.parent(), self.config_name, True)
        return response

    def on_order_book_update_message(self, latest_book: Book) -> None:
        self.books[latest_book.instrument] = latest_book
        if (
            len(self.books) != 2 or self.books[0].latest_seqno != self.books[1].latest_seqno
        ):
            return
        if time.time() < self.ts_next_plot:
            return
        self.replot_spread(latest_book)
        self.ts_next_plot = time.time() + Theme.DEFAULT_PLOT_DELAY_S

    @tperf
    def replot_spread(self, latest_book):
        self.graphWidget.clear()
        fut = self.books[0]
        etf = self.books[1]
        look_back = min(
            self.spinBox.value(), len(fut.tplot_x_seqno), len(etf.tplot_x_seqno)
        )
        if look_back == 0:
            return
        seqnos = fut.tplot_x_seqno[-look_back:]
        px_fut = numpy.array(fut.tplot_y_mid_px[-look_back:])
        px_etf = numpy.array(etf.tplot_y_mid_px[-look_back:])
        spread = numpy.subtract(px_fut, px_etf)
        eft_pos = etf.tplot_y_pos[-look_back:]
        self.graphWidget.plotItem.plot(seqnos, spread, pen=self.plot_pens[0])
        self.plt_vbox.addItem(pyqtgraph.PlotCurveItem(seqnos, eft_pos, pen=self.plot_pens[1]))

    def on_order_status_message(self, oid):
        pass


class PnlOverTimeGadget(Ui_TimeSeriesGadget, QTimeSeriesGadget):
    def __init__(self):
        self.config_name = None
        self.settings_loaded = False
        super().__init__()
        self.setupUi(self)
        self.ts_next_plot = 0
        self.graphWidget = pyqtgraph.PlotWidget(self)
        self.graphWidget.showGrid(True, True)
        self.verticalLayout_2.addWidget(self.graphWidget)
        self.spinBox.setRange(10, 1000000)
        self.spinBox.setSingleStep(10)
        self.spinBox.setValue(Theme.DEFAULT_LOOKBACK)
        self.btAutoScroll.clicked.connect(
            lambda: self.graphWidget.enableAutoRange(
                axis=None, enable=True, x=None, y=None
            )
        )
        self.setWindowTitle("Profit/Loss over time")
        self.graphWidget.setTitle("Profit/Loss")
        self.plot_pen = pyqtgraph.mkPen(color=Theme.COLOR_ETF, width=Theme.PWIDTH)
        self.config_name = "ProfitOverTime"
        self.tplot_x = []
        self.tplot_y = []
        window_config_map_load(self, self.config_name)

    def closeEvent(self, event):
        on_close_mdi_window(self, self.config_name)
        self.config_name = None

    def load_wnd_map(self):
        window_config_map_load(self.parent(), self.config_name)
        self.settings_loaded = True

    def event(self, ev):
        response = super().event(ev)
        if ev.type() == QtCore.QEvent.Resize or int(ev.type()) == 12:
            if self.config_name is not None and self.parent() is not None and self.settings_loaded:
                window_config_map_save(self.parent(), self.config_name, True)
        return response

    def on_order_book_update_message(self, latest_book: Book) -> None:
        if g_om.clt.team_id is None or latest_book.instrument != 1:
            return
        if time.time() < self.ts_next_plot:
            return
        self.replot_pnl(latest_book)
        self.ts_next_plot = time.time() + Theme.DEFAULT_PLOT_DELAY_S

    @tperf
    def replot_pnl(self, latest_book):
        my_acct = g_participants[g_om.clt.team_id]
        self.tplot_x.append(latest_book.latest_seqno)
        self.tplot_y.append(my_acct.pnl)
        look_back = self.spinBox.value()
        self.graphWidget.clear()
        seqnos = self.tplot_x[-look_back:]
        profit = self.tplot_y[-look_back:]
        self.graphWidget.plot(seqnos, profit, pen=self.plot_pen)

    def on_order_status_message(self, oid):
        pass


class OrderViewGadget(Ui_OrderViewGadget, QOrderViewGadget):
    def __init__(self):
        self.closing = False
        self.settings_loaded = False
        super().__init__()
        self.setupUi(self)
        self.logger = logging.getLogger("ORDVIEW")
        self.btCxlAll.clicked.connect(lambda: g_om.cancel_all(1, True, True, 0))
        self.btCxlBids.clicked.connect(lambda: g_om.cancel_all(1, False, True, 0))
        self.btCxlAsks.clicked.connect(lambda: g_om.cancel_all(1, True, False, 0))
        self.tableMyOrders.resizeColumnsToContents()
        self.tableMyOrders.viewport().installEventFilter(self)

    def load_wnd_map(self):
        window_config_map_load(self.parent(), "OrderView")
        self.settings_loaded = True

    def closeEvent(self, event):
        self.closing = True
        on_close_mdi_window(self, "OrderView")

    def event(self, ev):
        response = super().event(ev)
        if ev.type() == QtCore.QEvent.Resize or int(ev.type()) == 12:
            if not self.closing and self.parent() is not None and self.settings_loaded:
                window_config_map_save(self.parent(), "OrderView", True)
        return response

    def on_order_book_update_message(self, book: Book) -> None:
        return

    def on_order_status_message(self, oid):
        self.ov_reload()

    @tperf
    def ov_reload(self):
        self.tableMyOrders.setRowCount(len(g_om.orders))
        rr = len(g_om.orders) -1
        for (oid, order) in g_om.orders.items():
            self.tableMyOrders.setItem(rr, 0, QTableWidgetItem("%d" % (oid)))
            self.tableMyOrders.setItem(rr, 1, QTableWidgetItem(Theme.INSTR_NAME[order.instr]))
            self.tableMyOrders.setItem(rr, 2, QTableWidgetItem(Theme.STR_SIDE[order.side]))
            self.tableMyOrders.setItem(rr, 3, QTableWidgetItem("%d" % (order.qx)))
            self.tableMyOrders.setItem(rr, 4, QTableWidgetItem("%d" % (order.qx_org)))
            self.tableMyOrders.setItem(
                rr, 5, QTableWidgetItem("%.2f" % (order.px / 100.0))
            )
            itemCxlButton = QTableWidgetItem("Cancel")
            itemCxlButton.setBackground(Theme.COLOR_BK_CXL)
            self.tableMyOrders.setItem(rr, 6, itemCxlButton)
            rr -= 1

    def eventFilter(self, source, event):
        is_click = event.type() == QtCore.QEvent.MouseButtonPress
        if (
            is_click and source is self.tableMyOrders.viewport()
        ):
            if event.buttons() != QtCore.Qt.LeftButton:
                return super().eventFilter(source, event)
            item = self.tableMyOrders.itemAt(event.pos())
            if item is None or item.column() != 6:
                return super().eventFilter(source, event)
            oid_str = self.tableMyOrders.item(item.row(), 0).text()
            self.logger.info("Cancel oid=%s" % (oid_str))
            g_om.cancel_oid(int(oid_str))
        return super().eventFilter(source, event)


class TradeViewGadget(Ui_TradeViewGadget, QTradeViewGadget):
    def __init__(self):
        self.closing = False
        self.settings_loaded = False
        super().__init__()
        self.setupUi(self)
        self.table_trades.resizeColumnsToContents()
        self.table_trades.viewport().installEventFilter(self)

    def closeEvent(self, event):
        self.closing = True
        on_close_mdi_window(self, "TradeView")

    def event(self, ev):
        response = super().event(ev)
        if ev.type() == QtCore.QEvent.Resize or int(ev.type()) == 12:
            if not self.closing and self.parent() is not None and self.settings_loaded:
                window_config_map_save(self.parent(), "TradeView", True)
        return response

    def load_wnd_map(self):
        window_config_map_load(self.parent(), "TradeView")
        self.settings_loaded = True

    def on_order_book_update_message(self, book: Book) -> None:
        return

    def on_order_status_message(self, oid):
        self.tv_reload()

    @tperf
    def tv_reload(self):
        trades = g_om.trades[-Theme.TRADES_LOOKBACK:]
        self.table_trades.setRowCount(len(trades))
        rr = len(trades) - 1
        for trade in trades:
            order = trade.order
            self.table_trades.setItem(rr, 0, QTableWidgetItem("%d" % (trade.exch_time)))
            self.table_trades.setItem(rr, 1, QTableWidgetItem("%d" % (order.oid)))
            self.table_trades.setItem(rr, 2, QTableWidgetItem(Theme.STR_SIDE[order.side]))
            self.table_trades.setItem(rr, 3, QTableWidgetItem("%d" % (trade.trade_qx)))
            self.table_trades.setItem(rr, 4, QTableWidgetItem("%.2f" % (trade.trade_px/100.0)))
            rr -= 1


class PnlGadget(Ui_PnlGadget, QPnlGadget):
    def __init__(self):
        self.closing = False
        self.settings_loaded = False
        super().__init__()
        self.setupUi(self)
        self.tablePnl.resizeColumnsToContents()
        self.skip = -1

    def closeEvent(self, event):
        self.closing = True
        on_close_mdi_window(self, "TeamCompare")

    def event(self, ev):
        response = super().event(ev)
        if ev.type() == QtCore.QEvent.Resize or int(ev.type()) == 12:
            if not self.closing and self.parent() is not None and self.settings_loaded:
                window_config_map_save(self.parent(), "TeamCompare", True)
        return response

    def load_wnd_map(self):
        window_config_map_load(self.parent(), "TeamCompare")
        self.settings_loaded = True

    def on_order_book_update_message(self, book: Book) -> None:
        self.pnl_reload()

    def on_order_status_message(self, oid):
        pass

    @tperf
    def pnl_reload(self):
        self.skip += 1
        if self.skip == 10:
            self.skip = 0
        if self.skip != 0:
            return
        self.tablePnl.setRowCount(len(g_participants))
        rr = 0
        for (pid, player) in g_participants.items():
            self.tablePnl.setItem(rr, 0, QTableWidgetItem("%s" % (player.team_name)))
            self.tablePnl.setItem(
                rr, 1, QTableWidgetItem("%d" % (player.pnl))
            )  # Net Profit
            self.tablePnl.setItem(
                rr, 2, QTableWidgetItem("%d" % (player.balance))
            )  # Profit
            self.tablePnl.setItem(rr, 3, QTableWidgetItem("%d" % (player.pos_etf)))
            self.tablePnl.setItem(rr, 4, QTableWidgetItem("%d" % (player.fees)))
            rr += 1


async def app_main_loop_inner(a_loop, main_wnd):
    first_time = True
    while True:  # Retry connection
        loginwnd = LoginWindow(main_wnd)
        if not first_time and loginwnd.exec_() != QtWidgets.QDialog.Accepted:
            on_exit_app(1)

        on_con_lost = a_loop.create_future()
        try:
            transport, protocol = await a_loop.create_connection(
                lambda: ClickTradeClient(
                    a_loop,
                    on_con_lost,
                    loginwnd.username,
                    loginwnd.password,
                    main_wnd,
                ),
                loginwnd.server,
                loginwnd.port,
            )
            try:
                protocol.tcp_conn = transport
                await on_con_lost
            finally:
                transport.close()
            if protocol.exit_code == ExitCode.GRACEFUL:
                break
            elif protocol.exit_code == ExitCode.BAD_LOGIN:
                QtWidgets.QMessageBox.warning(loginwnd, "Error", "Bad user or password")
        except ConnectionRefusedError as ex:
            if not first_time:
                QtWidgets.QMessageBox.warning(loginwnd, "Error", "Could not connect")
        first_time = False


async def app_main_loop_outer(a_loop, main_wnd):
    try:
        await app_main_loop_inner(a_loop, main_wnd)
    except Exception as e:
        handle_exception(e)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print('Syntax: ./rt1_client.py <config.json>')
        exit(1)
    exit_code = 1
    try:
        logging.basicConfig(
            filename="rt1_client.log",
            format="%(asctime)s [%(levelname)-7s] [%(name)s] %(message)s",
            level=logging.INFO,
        )

        app = QtWidgets.QApplication(sys.argv)
        cfg = Configuration(sys.argv[1])
        app.setStyleSheet(Configuration.theme.qss)

        loop = QEventLoop(app)
        asyncio.set_event_loop(loop)

        main_wnd = MainWindow()
        main_wnd.show()

        loop.run_until_complete(app_main_loop_outer(loop, main_wnd))

        # ... Disconnected from server
        app.exec_()  # Keep UI up after disconnected
        exit_code = 0
    except Exception as ex:
        handle_exception(ex)
        exit_code = 1
    finally:
        Configuration.save()
        on_exit_app(exit_code)
