import sys
import os
import traceback
import json
import pathlib
import time

import socket
import asyncio
import struct
import random
import enum
from functools import reduce
from collections import deque
from typing import List, Optional, Text, Tuple, Union, Dict, Any

# Import Ready Trader messages
this_script_dir = os.path.dirname(os.path.realpath(__file__))
rt1_python_dir = os.path.abspath(os.path.join(this_script_dir, "../../server_exchange/"))
sys.path.insert(1, rt1_python_dir)
from ready_trader_one.messages import *
from ready_trader_one.order_book import TOP_LEVEL_COUNT
from ready_trader_one.types import Lifespan, Side


class JupyAlgoBase():
    def __init__(self):
        print('Algo starting...')
        self.next_oid = 1
        self.tcp_client = None
        self.team_name = None
        self.password = None
        self.hostname = None
        self.port = None
        self.loop = None
        self.print_ticks = True

    def on_order_book_update_message(
                self,
                instrument_id: int,
                sequence_number: int,
                ask_prices: List[int],
                ask_volumes: List[int],
                bid_prices: List[int],
                bid_volumes: List[int] ) -> None:
        if self.print_ticks:
            print('Book tick: instr=%d seq=%d best bid=%d best ask=%d' % \
                (instrument_id, sequence_number, bid_prices[0], ask_prices[0]))

    def on_trade_ticks_message(
        self, instrument: int, trade_ticks: List[Tuple[int, int]]):
        if self.print_ticks:
            print('Trade tick: instr=%d ticks=%s' % \
                (instrument, str(trade_ticks)))

    def on_error_message(self, client_order_id: int, error_message: bytes) -> None:
        print('Order [%d] error: %s' % (client_order_id, error_message))

    def on_order_status_message( self, oid: int, fill: int, remaining: int, fees: int,
            side, lifespan, org_price, fill_price):
        if oid >= self.next_oid:
            self.next_oid = oid + 1
        #print('Next OID= %d' % (self.next_oid))
        print('Order [%d] fill=%d remaining=%d fees=%d' % (oid, fill, remaining, fees))


    def on_position_change_message(self, future_position: int, etf_position: int):
        print('Position change FUT=%d ETF=%d ' % (future_position, etf_position))

    def send_insert_order(self, side_str, price: int, volume: int, lifespan: Lifespan = Lifespan.GOOD_FOR_DAY):
        if self.tcp_client == None:
            print('Not ready to send orders yet')
            return -1
        side = Side.BUY if side_str[:1].lower()=='b' else Side.SELL
        oid = self.next_oid
        self.next_oid += 1
        print('Sending new order: oid = %d, side = %s, price = %d, volume = %d, lifespan = %s' %\
            (oid, side, price, volume, lifespan))
        self.tcp_client.send_insert_order(oid, side, price, volume, lifespan)
        return oid

    def send_amend_order(self, oid: int, volume: int) -> None:
        if self.tcp_client == None:
            print('Not ready to amend orders yet')
            return
        print('Sending amend order: oid = %d, volume = %d' % (oid, volume))
        self.tcp_client.send_amend_order(oid, volume)

    def send_cancel_order(self, oid: int) -> None:
        if self.tcp_client == None:
            print('Not ready to cancel orders yet')
            return
        print('Sending cancel order: oid = %d' % (oid))
        self.tcp_client.send_cancel_order(oid)


def handle_exception(ex: Exception, context=None):
    xctx = "" if context is None or len(context) == 0 else (" while " + context)
    pbar = "#" * 120 + "\n"
    msg = pbar
    if type(ex) == Exception:  # Raised by us with no need for stacktrace
        msg += "Client threw an exception %s: %s\n" % (xctx, ex)
    else:  # Raised by a library and we'd like stracktrace info
        msg += "Client threw a '%s' exception %s: %s\n" % (xctx, type(ex).__name__, ex)
        msg += pbar
        traceback.print_exc(file=sys.stderr)
    print(msg, file=sys.stderr)
    exit(1)


class ExitCode(enum.IntEnum):
    GRACEFUL = 0
    COULD_NOT_CONNECT = 1
    BAD_LOGIN = 2
    CLIENT_COMM_ERROR = 3


class ClickTradeClient(asyncio.Protocol):
    def __init__( self, a_loop, a_on_con_lost, a_algo_obj: JupyAlgoBase ):
        self.on_con_lost = a_on_con_lost
        self.tcp_conn: Optional[asyncio.Transport] = None
        self.m_algo_obj = a_algo_obj

        self.team_id = None
        self.team_name: Optional[bytes] = a_algo_obj.team_name
        self.secret: Optional[bytes] = a_algo_obj.password
        self.exit_code = ExitCode.COULD_NOT_CONNECT

        self._data: bytes = b""
        self.amend_message: bytearray = bytearray(AMEND_MESSAGE_SIZE)
        self.cancel_message: bytearray = bytearray(CANCEL_MESSAGE_SIZE)
        self.insert_message: bytearray = bytearray(INSERT_MESSAGE_SIZE)
        self.BOOK_PART = struct.Struct("!%dI" % (TOP_LEVEL_COUNT,))
        HEADER.pack_into(
            self.amend_message, 0, AMEND_MESSAGE_SIZE, MessageType.AMEND_ORDER
        )
        HEADER.pack_into(
            self.cancel_message, 0, CANCEL_MESSAGE_SIZE, MessageType.CANCEL_ORDER
        )
        HEADER.pack_into(
            self.insert_message, 0, INSERT_MESSAGE_SIZE, MessageType.INSERT_ORDER
        )

    def connection_made(self, transport):  # from asyncio.Protocol
        message = HEADER.pack(
            LOGIN_MESSAGE_SIZE, MessageType.LOGIN
        ) + LOGIN_MESSAGE.pack(self.team_name.encode(), self.secret.encode(), True)
        transport.write(message)
        self.exit_code = ExitCode.BAD_LOGIN

    def exit_client(self, ec: ExitCode):
        self.exit_code = ec
        self.on_con_lost.set_result(True)

    def connection_lost(self, exc):  # from asyncio.Protocol
        print("The server closed the connection")
        self.on_con_lost.set_result(True)

    def on_clientstate_message(self, new_state: ClientState):
        if new_state == ClientState.LOGGED_IN:
            print("Login accepted")
            self.exit_code = ExitCode.GRACEFUL
        elif new_state == ClientState.TRADE_OPEN:
            print("Market open!")
        else:
            print("Unknown state: %s" % (new_state))

    def on_error_message(self, client_order_id: int, error_message: str):
        self.m_algo_obj.on_error_message(client_order_id, error_message)

    def on_position_change_message(
        self, future_position: int, etf_position: int
    ) -> None:
        self.m_algo_obj.on_position_change_message(future_position, etf_position)

    def on_order_book_update_message(
        self,
        instrument_id: int,
        sequence_number: int,
        ask_prices: List[int],
        ask_volumes: List[int],
        bid_prices: List[int],
        bid_volumes: List[int],
    ) -> None:
        self.m_algo_obj.on_order_book_update_message(instrument_id,sequence_number,ask_prices,ask_volumes,bid_prices,bid_volumes)

    def on_order_status_message(self, oid: int, fill: int, remaining: int, fees: int,
                            side, lifespan, org_price, fill_price):

        self.m_algo_obj.on_order_status_message(oid, fill, remaining, fees, side, lifespan, org_price, fill_price)

    def on_trade_ticks_message(
        self, instrument: int, trade_ticks: List[Tuple[int, int]]
    ) -> None:
        self.m_algo_obj.on_trade_ticks_message(instrument, trade_ticks)

    def send_amend_order(self, client_order_id: int, volume: int) -> None:
        AMEND_MESSAGE.pack_into(
            self.amend_message, HEADER_SIZE, client_order_id, volume
        )
        self.tcp_conn.write(self.amend_message)

    def send_cancel_order(self, client_order_id: int) -> None:
        CANCEL_MESSAGE.pack_into(self.cancel_message, HEADER_SIZE, client_order_id)
        self.tcp_conn.write(self.cancel_message)

    def send_insert_order(
        self,
        client_order_id: int,
        side: Side,
        price: int,
        volume: int,
        lifespan: Lifespan,
    ) -> None:
        INSERT_MESSAGE.pack_into(
            self.insert_message,
            HEADER_SIZE,
            client_order_id,
            side,
            price,
            volume,
            lifespan,
        )
        self.tcp_conn.write(self.insert_message)

    def on_new_participant(self, pid: int, team_name: str) -> None:
        if team_name == self.team_name:
            self.team_id = pid
        #g_participants[pid] = Participant(team_name)

    def on_pnl(self, pnl_items):
        #for pid, pos_etf, balance, pnl, fees in pnl_items:
            #g_participants[pid].update_pnl(pos_etf, balance, pnl, fees)
        pass

    def data_received(self, data):  # from asyncio.Protocol
        if self._data:
            self._data += data
        else:
            self._data = data
        try:
            upto = 0
            while upto < len(self._data) - HEADER_SIZE:
                length, typ = HEADER.unpack_from(self._data, upto)
                # log("inbound msg type=%d, size=%d" %(typ, length))
                if upto + length > len(self._data):
                    break  # break if we don't yet have enough data to consume the whole message

                if typ == MessageType.ERROR and length == ERROR_MESSAGE_SIZE:
                    client_order_id, error_message = ERROR_MESSAGE.unpack_from(
                        self._data, upto + HEADER_SIZE
                    )
                    self.on_error_message(
                        client_order_id, error_message.rstrip(b"\x00").decode("utf-8")
                    )

                elif (
                    typ == MessageType.ORDER_STATUS and length == ORDER_STATUS_MESSAGE_SIZE
                ):
                    self.on_order_status_message(
                        *ORDER_STATUS_MESSAGE.unpack_from(
                            self._data, upto + HEADER_SIZE
                        )
                    )

                elif (
                    typ == MessageType.POSITION_CHANGE and length == POSITION_CHANGE_MESSAGE_SIZE
                ):
                    self.on_position_change_message(
                        *POSITION_CHANGE_MESSAGE.unpack_from(
                            self._data, upto + HEADER_SIZE
                        )
                    )

                elif (
                    typ == MessageType.CLIENTSTATE and length == CLIENTSTATE_MESSAGE_SIZE
                ):
                    self.on_clientstate_message(
                        *CLIENTSTATE_MESSAGE.unpack_from(self._data, upto + HEADER_SIZE)
                    )

                elif (
                    typ == MessageType.ORDER_BOOK_UPDATE and length == ORDER_BOOK_MESSAGE_SIZE
                ):
                    inst, seq = ORDER_BOOK_HEADER.unpack_from(
                        self._data, upto + HEADER_SIZE
                    )
                    self.on_order_book_update_message(
                        inst,
                        seq,
                        *self.BOOK_PART.iter_unpack(
                            data[upto + ORDER_BOOK_HEADER_SIZE: upto + ORDER_BOOK_MESSAGE_SIZE]
                        )
                    )

                elif (
                    typ == MessageType.TRADE_TICKS and (
                        length - TRADE_TICKS_HEADER_SIZE) % TRADE_TICK_SIZE == 0
                ):
                    (inst,) = TRADE_TICKS_HEADER.unpack_from(
                        self._data, upto + HEADER_SIZE
                    )
                    ticks_nbytes = length - TRADE_TICKS_HEADER_SIZE
                    if ticks_nbytes % TRADE_TICK_SIZE != 0:
                        print(
                            "received TRADE_TICKS with unexpected padding. len=%d ticks_nbytes=%d",
                            length,
                            ticks_nbytes,
                        )
                        return self.exit_client(ExitCode.CLIENT_COMM_ERROR)
                    ticks = list(
                        TRADE_TICK.iter_unpack(
                            data[upto + TRADE_TICKS_HEADER_SIZE: upto + length]
                        )
                    )
                    self.on_trade_ticks_message(inst, ticks)

                elif (
                    typ == MessageType.NEW_PARTICIPANT and length == NEW_PARTICIPANT_MESSAGE_SIZE
                ):
                    pid, name = NEW_PARTICIPANT_MESSAGE.unpack_from(
                        self._data, upto + HEADER_SIZE
                    )
                    self.on_new_participant(pid, name.rstrip(b"\x00").decode("utf-8"))

                elif typ == MessageType.MARKET_PNL:
                    num_items = PNL_DATA_HEADER.unpack_from(
                        self._data, upto + HEADER_SIZE
                    )
                    pnls = list(
                        PNL_DATA.iter_unpack(
                            data[upto + PNL_DATA_HEADER_SIZE: upto + length]
                        )
                    )
                    self.on_pnl(pnls)

                else:
                    print(
                        "received invalid execution message: length=%d type=%d",
                        length,
                        typ,
                    )
                    return self.exit_client(ExitCode.CLIENT_COMM_ERROR)

                upto += length

        except BaseException as ex:
            handle_exception(ex, "receiving data")
            raise ex

        self._data = self._data[upto:]


async def app_main_loop_inner(a_loop, algo_obj):
    on_con_lost = a_loop.create_future()
    try:
        transport, protocol = await a_loop.create_connection(
            lambda: ClickTradeClient(
                a_loop,
                on_con_lost,
                algo_obj,
            ),
            algo_obj.hostname,
            algo_obj.port,
        )
        try:
            protocol.tcp_conn = transport
            algo_obj.tcp_client = protocol
            await on_con_lost
        finally:
            transport.close()
        if protocol.exit_code == ExitCode.GRACEFUL:
            return
        elif protocol.exit_code == ExitCode.BAD_LOGIN:
            print("Error: Bad team name or secret")
    except ConnectionRefusedError as ex:
        print( "Error: Connection refused (is the server up?)")


async def app_main_loop_outer(a_loop, algo_obj):
    try:
        await app_main_loop_inner(a_loop, algo_obj)
    except Exception as e:
        handle_exception(e)


def connect_algo_to_exchange(team_name, password, hostname, port, handler_algo_class):
    try:
        loop = asyncio.get_event_loop()
        asyncio.set_event_loop(loop)

        algo_obj = handler_algo_class()
        algo_obj.team_name = team_name
        algo_obj.password  = password
        algo_obj.hostname  = hostname
        algo_obj.port      = port
        algo_obj.loop      = loop

        loop.create_task(app_main_loop_outer(loop, algo_obj))
        
        # ... Disconnected from server

    except Exception as ex:
        handle_exception(ex)
 
    return algo_obj
